# tempSensor

Temperature and humidity sensor (SHT30) on a Arduino Wemos D1 mini.

## Installation

In the Arduino IDE, go to Preference and add *http://arduino.esp8266.com/stable/package_esp8266com_index.json* in the _Additional Board Manager URLs_.
Then go to _Tools->Board->Board Manager_ and search and install _ESP8266_.
